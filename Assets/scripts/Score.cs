﻿using TMPro;
using UnityEngine;

public class Score : MonoBehaviour
{
    private TextMeshPro text;

    private int score;

    void Start()
    {
        text = GetComponent<TextMeshPro>();
        GameManager.OnCubeSpawn += CubeSpawned;
    }

    public void CubeSpawned()
    {
        score++;
        text.text = score.ToString();
    }
}