﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MovingCube : MonoBehaviour
{
    public static MovingCube CurrentCube { get; private set; }
    public static MovingCube LastCube { get; set; }
    public MoveDirection MoveDirection { get; set; }

    [SerializeField] private float moveSpeed = 1f;
    private ParticleSystem system;
    bool moveDirectionZ = true;
    bool moveDirectionX = true;

    private void OnEnable()
    {
        if (LastCube == null)
            LastCube = GameObject.Find("Start").GetComponent<MovingCube>();
        transform.localScale = new Vector3(LastCube.transform.localScale.x, transform.localScale.y, LastCube.transform.localScale.z);
        CurrentCube = this;
    }


    void Update()
    {
        if (MoveDirection == MoveDirection.Z)
        {
            if (moveDirectionZ)
            {
                transform.position += transform.forward * (Time.deltaTime * moveSpeed);
                moveDirectionZ = !(transform.position.z > 1.5f);
            }
            else
            {
                transform.position -= transform.forward * (Time.deltaTime * moveSpeed);
                moveDirectionZ = !(transform.position.z > -1.5f);
            }
        }
        else
        {
            if (moveDirectionX)
            {
                transform.position += transform.right * (Time.deltaTime * moveSpeed);
                moveDirectionX = !(transform.position.x > 1.5f);
            }
            else
            {
                transform.position -= transform.right * (Time.deltaTime * moveSpeed);
                moveDirectionX = !(transform.position.x > -1.5f);
            }
        }
    }

    private void SpawnDropCubePart(float fallingBlockSize, float fallingBlockZPosition)
    {
        var cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
        var transform1 = transform;
        var localScale = transform1.localScale;
        if (MoveDirection == MoveDirection.Z)
        {
            cube.transform.localScale = new Vector3(localScale.x, localScale.y, fallingBlockSize);
            var position = transform1.position;
            cube.transform.position = new Vector3(position.x, position.y, fallingBlockZPosition);
        }
        else
        {
            cube.transform.localScale = new Vector3(fallingBlockSize, localScale.y, localScale.z);
            var position = transform1.position;
            cube.transform.position = new Vector3(fallingBlockZPosition, position.y, position.z);
        }

        cube.AddComponent<Rigidbody>();
        Destroy(cube.gameObject, 3f);
    }

    public void Stop(ParticleSystem system)
    {
        this.system = system;
        moveSpeed = 0f;
        var hangover = GetHangover();
        var lastScale = MoveDirection == MoveDirection.Z ? LastCube.transform.localScale.z : LastCube.transform.localScale.x;
        var direction = hangover > 0 ? 1f : -1f;
        if (Math.Abs(hangover) >= lastScale)
        {
            LastCube = null;
            CurrentCube = null;
            SceneManager.LoadScene(0);
        }
        else
        {
            if (MoveDirection == MoveDirection.Z)
                SliceOnZ(hangover, direction);
            else
                SliceOnX(hangover, direction);
            LastCube = this;
        }
    }

    private float GetHangover()
    {
        if (MoveDirection == MoveDirection.Z)
            return transform.position.z - LastCube.transform.position.z;
        return transform.position.x - LastCube.transform.position.x;
    }

    private void SliceOnZ(float hangover, float direction)
    {
        var localScale = transform.localScale;
        var position = transform.position;
        if (Math.Abs(hangover) <= 0.1f)
        {
            transform.localScale = new Vector3(
                LastCube.transform.localScale.x,
                localScale.y,
                LastCube.transform.localScale.z
            );
            transform.position = new Vector3(
                LastCube.transform.position.x,
                transform.position.y,
                LastCube.transform.position.z
            );
            system.Emit(1); //rework
        }
        else
        {
            var newSize = LastCube.transform.localScale.z - Math.Abs(hangover);
            var fallingBlockSize = localScale.z - newSize;

            var newPosition = LastCube.transform.position.z + hangover / 2;

            localScale = new Vector3(localScale.x, localScale.y, newSize);

            position = new Vector3(position.x, position.y, newPosition);
            transform.localScale = localScale;
            transform.position = position;

            var blockEdge = transform.position.z + (newSize / 2f * direction);
            var fallingBlockZPosition = blockEdge + (fallingBlockSize / 2f * direction);

            SpawnDropCubePart(fallingBlockSize, fallingBlockZPosition);
        }
    }

    private void SliceOnX(float hangover, float direction)
    {
        var localScale = transform.localScale;
        var position = transform.position;
        if (Math.Abs(hangover) <= 0.1f)
        {
            transform.localScale = new Vector3(
                LastCube.transform.localScale.x,
                localScale.y,
                LastCube.transform.localScale.z
            );
            transform.position = new Vector3(
                LastCube.transform.position.x,
                transform.position.y,
                LastCube.transform.position.z
            );
            system.Emit(1); //rework
        }
        else
        {
            var newSize = LastCube.transform.localScale.x - Math.Abs(hangover);
            var fallingBlockSize = localScale.x - newSize;

            var newPosition = LastCube.transform.position.x + hangover / 2;

            localScale = new Vector3(newSize, localScale.y, localScale.z);

            position = new Vector3(newPosition, position.y, position.z);
            transform.localScale = localScale;
            transform.position = position;

            var blockEdge = transform.position.x + (newSize / 2f * direction);
            var fallingBlockZPosition = blockEdge + (fallingBlockSize / 2f * direction);

            SpawnDropCubePart(fallingBlockSize, fallingBlockZPosition);
        }
    }
}