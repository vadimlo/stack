﻿using System;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static event Action OnCubeSpawn = delegate { };
    [SerializeField] private Camera camera;
    [SerializeField] private ParticleSystem particleLauncher;
    private Spawner[] spawners;
    private Spawner currentSpawner;
    private int spawnerIndex;
    private bool _isFirstClick = true;

    private void Awake()
    {
        spawners = FindObjectsOfType<Spawner>();
    }

    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            //new method
            if (!_isFirstClick)
                MovingCube.CurrentCube.Stop(particleLauncher);
            spawnerIndex = spawnerIndex == 0 ? 1 : 0;
            currentSpawner = spawners[spawnerIndex];
            currentSpawner.SpawnCube();
            _isFirstClick = false;
            //new method
            var transform1 = camera.transform;
            var position = transform1.position;
            camera.transform.position = Vector3.Lerp(
                position,
                new Vector3(position.x, position.y + MovingCube.CurrentCube.transform.localScale.y, position.z),
                1f);
            OnCubeSpawn();

            //new method
            var transform2 = MovingCube.LastCube.transform;
            var position1 = transform2.position;
            var transform3 = particleLauncher.transform;
            transform3.position = new Vector3(position1.x,
                position1.y - 0.04f, position1.z);
            var localScale = transform2.localScale;
            transform3.localScale = new Vector3(localScale.x + 0.1f,
                localScale.y, localScale.z + 0.1f);
            //particleLauncher.Emit(1);
        }
    }
}