﻿using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField] private MovingCube cubePrefab;
    [SerializeField] private MoveDirection moveDirection;

    public void SpawnCube()
    {
        var cube = Instantiate(cubePrefab);
        if (MovingCube.LastCube != null && MovingCube.LastCube.gameObject != GameObject.Find("Start"))
        {
            var position = transform.position;
            var position1 = MovingCube.LastCube.transform.position;
            float x = moveDirection == MoveDirection.X ? position.x : position1.x;
            float z = moveDirection == MoveDirection.Z ? position.z : position1.z;
            cube.transform.position = new Vector3(x,
                position1.y + cubePrefab.transform.localScale.y,
                z);
            cube.GetComponent<Renderer>().material.color = ColorChanger();
        }
        else
        {
            cube.transform.position = transform.position;
        }

        cube.MoveDirection = moveDirection;
    }

    private Color ColorChanger()
    {
        var col = MovingCube.LastCube.GetComponent<Renderer>().material.color;
        col.r -= 0.01f;
        col.g += 0.02f;
        col.b += 0.02f;
        return col;
    }
}